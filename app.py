from flask import Flask, jsonify, request

app = Flask('security-server')


@app.route('/oauth/login', methods=['POST'])
def oauth():
    body = request.get_json()
    text = body['username']
    search = body['password']
    response = {
          "access_token":"MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
          "token_type":"bearer",
          "expires_in":3600,
          "refresh_token":"IwOGYzYTlmM2YxOTQ5MGE3YmNmMDFkNTVk",
          "scope":"create"
    }
    return jsonify(response)

@app.route('/login/<username>/<password>', methods=['GET'])
def login(username, password):
    response = {
          "access_token":"MTQ0NjJkZmQ5OTM2NDE1ZTZjNGZmZjI3",
          "token_type":"bearer",
          "expires_in":3600,
          "refresh_token":"IwOGYzYTlmM2YxOTQ5MGE3YmNmMDFkNTVk",
          "scope":"create"
    }
    return jsonify(response)


if __name__ == '__main__':
    app.run()
